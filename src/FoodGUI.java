import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {

    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton hamburgerButton;
    private JButton yakinikuButton;
    private JButton curryButton;
    private JButton checkOutButton;
    private JButton drinkButton;
    private JTextArea list;
    private JPanel root;
    private JLabel price;

    int total = 0;

    void order(String food,int money ){

        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+" ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            String currentText = list.getText();
            list.setText(currentText + food + " " + money + " yen\n");
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering "+food+" !  It will be served as soon as possible.");
            list.getText();
            total += money;
            price.setText("Total "+ total + " yen.");
        }

    }

    public FoodGUI() {

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 1300 );
            }
        });

        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("tenpura.jpg")
        ));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 800);
            }
        });

        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("ramen.jpg")
        ));

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 600);
            }
        });

        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon.jpg")
        ));

        hamburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hamburger", 1100);
            }
        });

        hamburgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("humburger.jpg")
        ));

        yakinikuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakiniku", 2500);
            }
        });

        yakinikuButton.setIcon(new ImageIcon(
                this.getClass().getResource("yakiniku.jpg")
        ));

        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Curry", 1000);
            }
        });

        curryButton.setIcon(new ImageIcon(
                this.getClass().getResource("curry.jpg")
        ));

        drinkButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Drink", 400);
            }
        });

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout ?",
                        "Checkout Confirm",
                        JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    JOptionPane.showConfirmDialog(null,
                            "Thank you. The total price is " + total + " yen.");
                    list.setText("");
                    price.setText("Total 0 yen.");
                    total = 0;
                }
            }
        });

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
